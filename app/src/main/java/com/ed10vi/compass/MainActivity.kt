package com.ed10vi.compass

import android.annotation.SuppressLint
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updateLayoutParams
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.ed10vi.compass.databinding.ActivityMainBinding
import com.google.android.filament.Renderer
import com.google.android.filament.View
import com.google.android.material.elevation.SurfaceColors
import dev.romainguy.kotlin.math.*
import io.github.sceneview.loaders.EnvironmentLoader
import io.github.sceneview.math.Position
import io.github.sceneview.math.colorOf
import io.github.sceneview.node.ModelNode
import kotlinx.coroutines.launch


class MainActivity : AppCompatActivity(), SensorEventListener {

    private lateinit var binding: ActivityMainBinding
    private lateinit var sensorManager: SensorManager
    private var south: Float3 = Float3(0f, 0f, 1f)
    private var up: Float3 = Float3(0f, 1f, 0f)

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        WindowCompat.setDecorFitsSystemWindows(window, false)

        ViewCompat.setOnApplyWindowInsetsListener(binding.uiLayout) { view, windowInsets ->
            val insets = windowInsets.getInsets(WindowInsetsCompat.Type.systemBars())
            view.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                leftMargin = insets.left
                rightMargin = insets.right
                topMargin = insets.top
                bottomMargin = insets.bottom
            }
            WindowInsetsCompat.CONSUMED
        }

        sensorManager = getSystemService(SensorManager::class.java)

        val sceneView = binding.sceneView
        val bgColor = SurfaceColors.getColorForElevation(this, sceneView.elevation)

        sceneView.lifecycle = lifecycle
        sceneView.setZOrderMediaOverlay(true)
        sceneView.view.blendMode = View.BlendMode.TRANSLUCENT
        sceneView.renderer.clearOptions = Renderer.ClearOptions().apply {
            clear = true
            clearColor = colorOf(bgColor).toFloatArray()
        }
        sceneView.skybox = null
        sceneView.setOnTouchListener { _, _ -> true }

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.CREATED) {
                sceneView.indirectLight = EnvironmentLoader(
                    sceneView.engine,
                    this@MainActivity
                ).createHDREnvironment("environments/studio_small_09_2k.hdr")?.indirectLight

                val model = sceneView.modelLoader.loadModelInstance("models/compass.glb")!!
                val mn = ModelNode(model).apply {
                    transform(Position(0f, 0f, -5f), Quaternion())
                }
                sceneView.addChildNode(mn)

                sceneView.onFrame = {
                    val west = south x up
                    mn.transform = lookTowards(mn.position, south, normalize(west x south))
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        // sampling period in µs
        val sampling = (1000000 / binding.sceneView.renderer.displayInfo.refreshRate).toInt()
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), sampling)
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY), sampling)
    }

    override fun onPause() {
        super.onPause()
        sensorManager.unregisterListener(this)
    }

    override fun onSensorChanged(data: SensorEvent) {
        val v: Float3
        when (data.sensor.type) {
            Sensor.TYPE_MAGNETIC_FIELD -> {
                v = Float3(data.values[0], data.values[1], data.values[2])
                south = normalize(v)
                binding.infoView.text = getString(R.string.mag_format, length(v))
            }
            Sensor.TYPE_GRAVITY -> {
                v = Float3(data.values[0], data.values[1], data.values[2])
                up = normalize(v)
            }
        }
    }

    override fun onAccuracyChanged(s: Sensor, ac: Int) {
        val indeterminate: Boolean
        if (s.type == Sensor.TYPE_MAGNETIC_FIELD) {
            indeterminate = ac > SensorManager.SENSOR_STATUS_ACCURACY_HIGH
            binding.accuracy.isIndeterminate = indeterminate
            binding.accuracy.setProgress(when (ac) {
                SensorManager.SENSOR_STATUS_UNRELIABLE -> 0
                SensorManager.SENSOR_STATUS_ACCURACY_LOW -> 33
                SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM -> 66
                SensorManager.SENSOR_STATUS_ACCURACY_HIGH -> 100
                else -> 0
            }, !indeterminate)
        }
    }
}
